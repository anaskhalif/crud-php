<?php 
require 'fungsi.php';


if( isset($_POST["submit"]) ) {

	if( tambah($_POST) > 0 ) {
		echo "<script>
		alert('data berhasil di tambahkan');
		document.location.href = 'index.php'
		</script>";
	} else {
		echo "<script>
		alert('data gagal di tambahkan');
		document.location.href = 'index.php';
		</script>";
	}
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<title>CRUD APP</title>
</head>
<body>
	<nav class="navbar navbar-light justify-content-center fs-3 mb-5"
	style="background-color: #8A2BE2">
		PHP CRUD Application
	</nav>

	<div class="container">
		<div class="text-center mb-4">
			<h3>Tambahkan User</h3>
			<p class="text-muted">Lengkapi formulir di bawah ini untuk menambahkan pengguna baru</p>	
		</div>

		<div class="container d-flexd justify-content-center">
			<form action="" method="post">
				<div class="row">
					<div class="col">
						<label for="nama_depan">Nama Depan :</label>
						<input type="text" name="nama_depan"id="nama_depan" class="form-control" placeholder="Masukan Nama Depan" required>
					</div>

					<div class="col">
						<label for="nama_belakang">Nama Belakang :</label>
						<input type="text" name="nama_belakang" id="nama_belakang" class="form-control" placeholder="Masukan nama belakang" required>
					</div>
				</div>

				<div class="mb-3">
					<label for="email">Email : </label>
					<input type="text" name="email" id="email" class="form-control" placeholder="Masukan Email" required>
				</div>

				<div class="form-group mb-3">
					<label>Jenis Kelamin : </label> &nbsp;
					<input type="radio" class="form-check-input" name="gender" id="pria" value="pria">
					<label for="pria" class="form-check-label">Pria</label>
					&nbsp;
					<input type="radio" class="form-check-input" name="gender" id="wanita" value="wanita">
					<label for="wanita" class="form-check-label">Wanita</label>
				</div>

				<div>
					<button type="submit" class="btn btn-success" name="submit">Save</button>
					<a href="index.php" class="btn btn-danger">Cancel</a>
				</div>
			</form>
		</div>
	</div>
	
</body>
</html>